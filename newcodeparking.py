from nanpy import ArduinoApi
from nanpy import SerialManager
from nanpy import Ultrasonic
from nanpy import Tone
from time import sleep
import I2C_LCD_driver
import RPi.GPIO as GPIO
from time import *

GPIO.setmode(GPIO.BOARD)

#Drive motor
Motor1A = 13 # set GPIO-02 as Input 1 of the controller IC
Motor1B = 11 # set GPIO-03 as Input 2 of the controller IC
Motor1E = 15 # set GPIO-04 as Enable pin 1 of the controller IC
#Steering motor
Motor2A = 18
Motor2B = 19

GPIO.setup(Motor1A,GPIO.OUT)
GPIO.setup(Motor1B,GPIO.OUT)
GPIO.setup(Motor1E,GPIO.OUT)
GPIO.setup(Motor2A,GPIO.OUT)
GPIO.setup(Motor2B,GPIO.OUT)

motor1=GPIO.PWM(15,200) # configuring Enable pin means GPIO-04 for PWM
motor1.start(0)
motor1.ChangeDutyCycle(0)
# Creating variables refering to the pins assigned to the sensors in the Arduino

# Ultrasonic Sensors
trigPin1 = 4
echoPin1 = 5
trigPin2 = 2
echoPin2 = 3
trigPin3 = 6
echoPin3 = 7
trigPin4 = 14
echoPin4 = 15
trigPin5 = 12
echoPin5 = 13

buzz_pin = 8

# setting up lcd object
mylcd = I2C_LCD_driver.lcd()

# Test if Arduino is connected to Raspberry pi
try:
    connection = SerialManager(device='/dev/ttyACM0')
    a = ArduinoApi(connection=connection)

except:
    print("Failed to connect to Arduino")

# creating objects of 'Ultrasonic' to access the sensor data from each individual
# ultrasonic sensors. The Ultrasonic pkg in nanpy retrives distance data from the
# Arduino.

ultrasonic_1 = Ultrasonic(echoPin1, trigPin1, False, connection=connection) # front left
ultrasonic_2 = Ultrasonic(echoPin2, trigPin2, False, connection=connection) # front right
ultrasonic_3 = Ultrasonic(echoPin3, trigPin3, False, connection=connection) # Left
ultrasonic_4 = Ultrasonic(echoPin4, trigPin4, False, connection=connection) # right
ultrasonic_5 = Ultrasonic(echoPin5, trigPin5, False, connection=connection) # rear

buzz_tone = Tone(buzz_pin)
buzz_tone.stop()

GPIO.output(Motor1A,GPIO.LOW)
GPIO.output(Motor1B,GPIO.LOW)
GPIO.output(Motor2A,GPIO.LOW)
GPIO.output(Motor2B,GPIO.LOW)

def allignment():
    # measure ultrasonic reading on all sides
    print("Alligning the vehicle to front wall")
    mylcd.lcd_clear()
    mylcd.lcd_display_string("ALLIGNING",1,3)
    mylcd.lcd_display_string(" TO FRONT",2,4)
    distance1 = ultrasonic_1.get_distance()
    distance2 = ultrasonic_2.get_distance()
    distance3 = ultrasonic_3.get_distance()
    distance4 = ultrasonic_4.get_distance()
    distance5 = ultrasonic_5.get_distance()
    print("front left :",distance1,"front right :",distance2)


    # Checking for allignment
    # Distance to the front left and right
    while distance1 > 10 or distance2 > 10:

        GPIO.output(Motor1A,GPIO.LOW)
        GPIO.output(Motor1B,GPIO.LOW)
        GPIO.output(Motor2A,GPIO.LOW)
        GPIO.output(Motor2B,GPIO.LOW)
        sleep(0.5)

        if abs(distance1 - distance2) < 2:

            print("vehicle alligned, moving forward")
            print("front left :",distance1,"front right :",distance2)
            mylcd.lcd_clear()
            mylcd.lcd_display_string("ALLIGNED",1,3)
            mylcd.lcd_display_string("FORWARD",2,3)

            GPIO.output(Motor1A,GPIO.HIGH)
            GPIO.output(Motor1B,GPIO.LOW)
            motor1.ChangeDutyCycle(60)
            sleep(0.5)

        else:
            if distance2 < distance1:

                print("reversing and alligning to left")
                print("front left :",distance1,"front right :",distance2)
                mylcd.lcd_clear()
                mylcd.lcd_display_string("ALLIGN LEFT",1,3)
                mylcd.lcd_display_string("REVERSE",2,3)

                GPIO.output(Motor2A,GPIO.HIGH)
                GPIO.output(Motor2B,GPIO.LOW)
                sleep(0.5)

                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                sleep(0.2)
                buzz_tone.stop()
            else:

                print("reversing and alligning to right")
                print("front left :",distance1,"front right :",distance2)
                mylcd.lcd_clear()
                mylcd.lcd_display_string("ALLIGN RIGHT",1,3)
                mylcd.lcd_display_string("REVERSE",2,3)
                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.HIGH)
                sleep(0.5)

                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                sleep(0.2)
                buzz_tone.stop()


        distance1 = ultrasonic_1.get_distance()
        distance2 = ultrasonic_2.get_distance()

    print("exiting allignment")
    mylcd.lcd_clear()
    mylcd.lcd_display_string("ALLIGNED AND",1,3)
    mylcd.lcd_display_string("POSITIONED",2,3)
    GPIO.output(Motor2A,GPIO.LOW)
    GPIO.output(Motor2B,GPIO.LOW)
    GPIO.output(Motor1A,GPIO.LOW)
    GPIO.output(Motor1B,GPIO.LOW)
    motor1.ChangeDutyCycle(0)

def parking():

    distance3 = ultrasonic_3.get_distance()
    distance4 = ultrasonic_4.get_distance()

    # Distance to the left and right
    while distance3 > 10 and distance4 > 10:
        GPIO.output(Motor2A,GPIO.LOW)
        GPIO.output(Motor2B,GPIO.LOW)
        GPIO.output(Motor1A,GPIO.LOW)
        GPIO.output(Motor1B,GPIO.LOW)
        sleep(0.5)

        if distance3 < distance4:
            # normal side shift
            if distance3 > 25:
                print("parking to the left")
                print("left dist:",distance3,"right dist:",distance4)
                mylcd.lcd_clear()
                mylcd.lcd_display_string("PARKING LEFT",1,1)
                sleep(0.2)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.HIGH)
                GPIO.output(Motor2B,GPIO.LOW)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("LEFT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.3)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.HIGH)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("RIGHT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.3)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)
            # small side shift
            elif distance3 > 17:
                print("parking to the left")
                print("left dist:",distance3,"right dist:",distance4)
                mylcd.lcd_clear()
                mylcd.lcd_display_string("PARKING LEFT",1,1)
                sleep(0.2)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.HIGH)
                GPIO.output(Motor2B,GPIO.LOW)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("LEFT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.2)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.HIGH)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("RIGHT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.2)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)
            # smaller side shift
            else:
                print("parking to the left")
                print("left dist:",distance3,"right dist:",distance4)
                mylcd.lcd_clear()
                mylcd.lcd_display_string("PARKING LEFT",1,1)
                sleep(0.2)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.HIGH)
                GPIO.output(Motor2B,GPIO.LOW)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("LEFT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.1)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.HIGH)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("RIGHT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.1)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)

            allignment()

        else:
            # normal side shift
            if distance4 > 25:
                print("parking to the right")
                print("left dist:",distance3,"right dist:",distance4)
                mylcd.lcd_clear()
                mylcd.lcd_display_string("PARKING RIGHT",1,1)
                sleep(0.2)

                print("turn wheel to right")
                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.HIGH)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("RIGHT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.3)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.HIGH)
                GPIO.output(Motor2B,GPIO.LOW)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("LEFT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.3)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)
            # small side shift
            elif distance3 > 17:
                print("parking to the right")
                print("left dist:",distance3,"right dist:",distance4)
                mylcd.lcd_clear()
                mylcd.lcd_display_string("PARKING RIGHT",1,1)
                sleep(0.2)

                print("turn wheel to right")
                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.HIGH)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("RIGHT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.2)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.HIGH)
                GPIO.output(Motor2B,GPIO.LOW)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("LEFT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.2)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)
            # smaller side shift
            else:
                print("parking to the right")
                print("left dist:",distance3,"right dist:",distance4)
                mylcd.lcd_clear()
                mylcd.lcd_display_string("PARKING RIGHT",1,1)
                sleep(0.2)

                print("turn wheel to right")
                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.HIGH)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("RIGHT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.1)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)

                print("turn wheel to left")
                GPIO.output(Motor2A,GPIO.HIGH)
                GPIO.output(Motor2B,GPIO.LOW)
                sleep(0.5)

                print("reversing")
                mylcd.lcd_clear()
                mylcd.lcd_display_string("LEFT REVERSE",1,1)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.HIGH)
                motor1.ChangeDutyCycle(80)

                for i in range(3):
                    buzz_tone.play(buzz_tone.NOTE_B5,0.2)
                    buzz_tone.stop()
                    sleep(0.1)

                GPIO.output(Motor2A,GPIO.LOW)
                GPIO.output(Motor2B,GPIO.LOW)
                GPIO.output(Motor1A,GPIO.LOW)
                GPIO.output(Motor1B,GPIO.LOW)
                sleep(0.5)

            allignment()

        distance3 = ultrasonic_3.get_distance()
        distance4 = ultrasonic_4.get_distance()
        GPIO.output(Motor2A,GPIO.LOW)
        GPIO.output(Motor2B,GPIO.LOW)
        GPIO.output(Motor1A,GPIO.LOW)
        GPIO.output(Motor1B,GPIO.LOW)
        motor1.ChangeDutyCycle(0)
        sleep(0.5)

if __name__ == '__main__':
    allignment()
    parking()
    distance1 = ultrasonic_1.get_distance()
    distance2 = ultrasonic_2.get_distance()

    while distance1 > 3 or distance2 > 3:

        print("parking the vehicle")
        print("front left :",distance1,"front right :",distance2)
        mylcd.lcd_clear()
        mylcd.lcd_display_string("PARKING",1,3)
#        GPIO.output(Motor1B,GPIO.LOW)
#        GPIO.output(Motor2A,GPIO.LOW)
#        GPIO.output(Motor2B,GPIO.LOW)
#        sleep(0.5)

        GPIO.output(Motor1A,GPIO.HIGH)
        GPIO.output(Motor1B,GPIO.LOW)
        motor1.ChangeDutyCycle(30)
        distance1 = ultrasonic_1.get_distance()
        distance2 = ultrasonic_2.get_distance()
    GPIO.output(Motor1A,GPIO.LOW)
    GPIO.output(Motor1B,GPIO.LOW)
    GPIO.output(Motor2A,GPIO.LOW)
    GPIO.output(Motor2B,GPIO.LOW)
    mylcd.lcd_clear()
    mylcd.lcd_display_string("VEHICLE PARKED",2,1)
    sleep(0.5)
