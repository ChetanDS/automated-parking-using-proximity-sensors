from nanpy import ArduinoApi
from nanpy import SerialManager
from nanpy import Ultrasonic
from nanpy import Tone
from time import sleep
import I2C_LCD_driver
import RPi.GPIO as GPIO
from time import *


GPIO.setmode(GPIO.BOARD)

#Drive motor
Motor1A = 13 # set GPIO-02 as Input 1 of the controller IC
Motor1B = 11 # set GPIO-03 as Input 2 of the controller IC
Motor1E = 15 # set GPIO-04 as Enable pin 1 of the controller IC
#Steering motor
Motor2A = 18
Motor2B = 19

GPIO.setup(Motor1A,GPIO.OUT)
GPIO.setup(Motor1B,GPIO.OUT)
GPIO.setup(Motor1E,GPIO.OUT)
GPIO.setup(Motor2A,GPIO.OUT)
GPIO.setup(Motor2B,GPIO.OUT)

motor1=GPIO.PWM(15,100) # configuring Enable pin means GPIO-04 for PWM
motor1.start(0)
motor1.ChangeDutyCycle(0)

# Creating variables refering to the pins assigned to the sensors in the Arduino

# Ultrasonic Sensors
trigPin1 = 4
echoPin1 = 5
trigPin2 = 2
echoPin2 = 3
trigPin3 = 6
echoPin3 = 7
trigPin4 = 14
echoPin4 = 15
trigPin5 = 12
echoPin5 = 13

# IR Sensors
ir_pin1 = 16
ir_pin2 = 17
ir_pin3 = 18
ir_pin4 = 19

# Piezobuzzer
buzz_pin = 8

# setting up lcd object
mylcd = I2C_LCD_driver.lcd()

# Test if Arduino is connected to Raspberry pi
try:
    connection = SerialManager(device='/dev/ttyACM0')
    a = ArduinoApi(connection=connection)

except:
    print("Failed to connect to Arduino")

# creating objects of 'Ultrasonic' to access the sensor data from each individual
# ultrasonic sensors. The Ultrasonic pkg in nanpy retrives distance data from the
# Arduino.

ultrasonic_1 = Ultrasonic(echoPin1, trigPin1, False, connection=connection)
ultrasonic_2 = Ultrasonic(echoPin2, trigPin2, False, connection=connection)
ultrasonic_3 = Ultrasonic(echoPin3, trigPin3, False, connection=connection)
ultrasonic_4 = Ultrasonic(echoPin4, trigPin4, False, connection=connection)
ultrasonic_5 = Ultrasonic(echoPin5, trigPin5, False, connection=connection)

# Assigning the function of pins connected to the IR sensor. The IR sensor package
# part of the ArduinoApi pkg

a.pinMode(ir_pin1, a.INPUT)
a.pinMode(ir_pin2, a.INPUT)
a.pinMode(ir_pin3, a.INPUT)
a.pinMode(ir_pin4, a.INPUT)

# Creating object of Tone pkg in nanpy

buzz_tone = Tone(buzz_pin)
buzz_tone.stop()

GPIO.output(Motor1A,GPIO.LOW)
GPIO.output(Motor1B,GPIO.LOW)
GPIO.output(Motor2A,GPIO.LOW)
GPIO.output(Motor2B,GPIO.LOW)

# Function for collision warning

def test():

    # Obtain the obstacle distance data from each ultrasonic sensor

    distance1 = ultrasonic_1.get_distance()
    distance2 = ultrasonic_2.get_distance()
    distance3 = ultrasonic_3.get_distance()
    distance4 = ultrasonic_4.get_distance()
    distance5 = ultrasonic_5.get_distance()

    # Reading obstacle presence information from IR sensors

    ir_sensorOut1 = a.digitalRead(ir_pin1)
    ir_sensorOut2 = a.digitalRead(ir_pin2)
    ir_sensorOut3 = a.digitalRead(ir_pin3)
    ir_sensorOut4 = a.digitalRead(ir_pin4)

    print("Front :", distance1,distance2,"Right :", distance4, \
    "Left :", distance3,"Rear :", distance5)
    mylcd.lcd_clear()

    # Based on the object distance, The system warning changes intensity

    # Ultrasonic sensor 1: Front facing
    if distance1 < 25 or distance2 < 25:
        mylcd.lcd_clear()
        print('Obstacle to the FRONT!')
        mylcd.lcd_display_string("ATTENTION",1,1)
        mylcd.lcd_display_string("FRONT",2,4)
        buzz_tone.play(buzz_tone.NOTE_B5,0.2)
        buzz_tone.stop()
        sleep(0.2)


    elif distance1 < 20 or distance2 < 20:
        mylcd.lcd_clear()
        print('Obstacle close to the FRONT!')
        mylcd.lcd_display_string("CAUTION",1,2)
        mylcd.lcd_display_string("FRONT",2,4)
        buzz_tone.play(buzz_tone.NOTE_B5,0.2)
        buzz_tone.stop()
        sleep(0.2)


    elif distance1 < 15 or distance2 < 15:
        mylcd.lcd_clear()
        print('Obstacle too close to the FRONT!')
        mylcd.lcd_display_string("WARNING",1,3)
        mylcd.lcd_display_string("FRONT",2,4)
        buzz_tone.play(buzz_tone.NOTE_B5,0.2)
        buzz_tone.stop()
        sleep(0.2)


     # Ultrasonic sensor 2: Right facing
    if distance4 < 15:
        mylcd.lcd_clear()
        print('too close to the RIGHT!')
        mylcd.lcd_display_string("ATTENTION",1,2)
        mylcd.lcd_display_string("RIGHT",2,4)
        buzz_tone.play(buzz_tone.NOTE_A5,0.2)
        buzz_tone.stop()
        sleep(0.2)

    elif distance4 < 12:
        mylcd.lcd_clear()
        print('Obstacle close to the RIGHT!')
        mylcd.lcd_display_string("CAUTION",1,2)
        mylcd.lcd_display_string("RIGHT",2,4)
        buzz_tone.play(buzz_tone.NOTE_B5,0.3)
        buzz_tone.stop()
        sleep(0.2)

    elif distance4 < 10:
        mylcd.lcd_clear()
        print('Obstacle too close to the RIGHT!')
        mylcd.lcd_display_string("WARNING",1,2)
        mylcd.lcd_display_string("RIGHT",2,4)
        buzz_tone.play(buzz_tone.NOTE_D6,0.4)
        buzz_tone.stop()
        sleep(0.2)

    # Ultrasonic sensor 1: Rear facing
    if distance5 < 25:
        mylcd.lcd_clear()
        print('too close to the REAR!')
        mylcd.lcd_display_string("ATTENTION",1,2)
        mylcd.lcd_display_string("REAR",2,4)
        buzz_tone.play(buzz_tone.NOTE_A5,0.2)
        buzz_tone.stop()
        sleep(0.2)

    elif distance5 < 20:
        mylcd.lcd_clear()
        print('Obstacle close to the REAR!')
        mylcd.lcd_display_string("CAUTION",1,2)
        mylcd.lcd_display_string("REAR",2,4)
        buzz_tone.play(buzz_tone.NOTE_B5,0.3)
        buzz_tone.stop()
        sleep(0.2)

    elif distance5 < 15:
        mylcd.lcd_clear()
        print('Obstacle too close to the REAR!')
        mylcd.lcd_display_string("WARNING",1,3)
        mylcd.lcd_display_string("REAR",2,4)
        buzz_tone.play(buzz_tone.NOTE_D6,0.4)
        buzz_tone.stop()
        sleep(0.2)

    # Ultrasonic sensor 1: Left facing
    if distance3 < 10:
        mylcd.lcd_clear()
        print('too close to the LEFT!')
        mylcd.lcd_display_string("ATTENTION",1,2)
        mylcd.lcd_display_string("FRONT",2,4)
        buzz_tone.play(buzz_tone.NOTE_A5,0.2)
        buzz_tone.stop()
        sleep(0.2)

    elif distance3 < 12:
        mylcd.lcd_clear()
        print('Obstacle close to the LEFT!')
        mylcd.lcd_display_string("CAUTION",1,2)
        mylcd.lcd_display_string("LEFT",2,4)
        buzz_tone.play(buzz_tone.NOTE_B5,0.3)
        buzz_tone.stop()
        sleep(0.2)

    elif distance3 < 10:
        mylcd.lcd_clear()
        print('Obstacle too close to the LEFT!')
        mylcd.lcd_display_string("WARNING",1,2)
        mylcd.lcd_display_string("LEFT",2,4)
        buzz_tone.play(buzz_tone.NOTE_D6,0.4)
        buzz_tone.stop()
        sleep(0.2)

       # IR Sensor: Front right facing
    if ir_sensorOut2 == a.LOW:
        mylcd.lcd_clear()
        print('Object at Front Right corner')
        mylcd.lcd_display_string("WARNING",1,3)
        mylcd.lcd_display_string("FRT RIGHT",2,2)
        buzz_tone.play(buzz_tone.NOTE_D6,0.2)
        buzz_tone.stop()
        sleep(0.2)

     # IR Sensor: Front left facing
    if ir_sensorOut1 == a.LOW:
        mylcd.lcd_clear()
        print('Object at Front Left corner')
        mylcd.lcd_display_string("WARNING",1,4)
        mylcd.lcd_display_string("FRT LEFT",2,1)
        buzz_tone.play(buzz_tone.NOTE_D6,0.2)
        buzz_tone.stop()
        sleep(0.2)

     # IR Sensor: Front right facing
    if ir_sensorOut4 == a.LOW:
        mylcd.lcd_clear()
        print('Object at Rear Right corner')
        mylcd.lcd_display_string("WARNING",1,4)
        mylcd.lcd_display_string("REAR RIGHT",2,1)
        buzz_tone.play(buzz_tone.NOTE_D6,0.2)
        buzz_tone.stop()
        sleep(0.2)

     # IR Sensor: Front left facing
    if ir_sensorOut3 == a.LOW:
        mylcd.lcd_clear()
        print('Object at Rear Left corner')
        mylcd.lcd_display_string("WARNING",1,4)
        mylcd.lcd_display_string("REAR LEFT",2,1)
        buzz_tone.play(buzz_tone.NOTE_D6,0.2)
        buzz_tone.stop()
        sleep(0.2)

    mylcd.lcd_clear()
    sleep(0.1)

if __name__ == '__main__':
    # Motor to move forward with duty cycle frequency of 50
    GPIO.output(Motor1A,GPIO.HIGH)
    GPIO.output(Motor1B,GPIO.LOW)
    motor1.ChangeDutyCycle(40)

    # Run the collision warning function (test()) in infinite loop
    while True:
        test()
        sleep(1)

    # All motors to stop
    #GPIO.output(Motor2A,GPIO.LOW)
    #GPIO.output(Motor2B,GPIO.LOW)
    #GPIO.output(Motor1A,GPIO.LOW)
    #GPIO.output(Motor1B,GPIO.LOW)
    #motor1.ChangeDutyCycle(0)